





## Items to Consider



## App Notes
- Although a best effort was made to ensure that the app is responsive (i.e. mobile friendly) and cross-browser compatible, no guarantees are provided.
- It has not been thoroughly tested, it may contain bugs or lack desired features.
- It was minimally tested on Google Chrome Version 70.0.3538.110 (Official Build) (64-bit).


## Getting Started
If you have Docker installed on your development computer you can follow these steps:

1) In Docker terminal window navigate to your desired development directory and enter:  
    ```git clone https://joelvanpatten@bitbucket.org/joelvanpatten/larafunk.git```

2) Navigate to the **larafunk** directory.  
    ```cd larafunk```

3) Copy the example environment file.  
    ```cp .env.example .env```  
    This is a demo application, so no changes to the .env file are required.  If this were a real application you would want to edit the .env file to fit your desired environment.  At a minimum you would want to change the APP_KEY.  Remember not to include passwords or sensitive account information in any file that is in version control (the .env file should remain in your .gitignore file list so that git will not add it to the repository).
    
4) Run docker-compose:  
    ```docker-compose up -d```  
    It will take a few moments for Docker to pull the desired images and build your containers.  Even after it appears that Docker is done, Composer is likely still pulling project dependencies into the container.  **Please allow a few moments for Docker to do its thing.**

5) If docker completed with no errors you should be able to view the demo application at [http://0.0.0.0:3000](http://0.0.0.0:3000).  
If you are using Docker Toolbox for Windows you may need to use [http://192.168.99.100:3000](http://192.168.99.100:3000).  
You could also try [http://localhost:3000](http://localhost:3000).

**Troubleshooting:**    
You can view the status of containers by first getting the container id:  
```docker ps```  
Then you can use the first few characters of the container id to view the container logs.  If your Laravel container id is **cc898eb10af**, then the command to view logs would be:  
```docker logs cc898e```  
You just need enough of the container id to uniquely identify it.

    
## Possible Improvements
